import json
import random
import re
import requests
from datetime import datetime, timezone, timedelta
from lxml import etree
# import schedule
import tools
import generate_thoughts as thoughts
# import time
import login
import sys


def main():
    global isLogin, to_mail_user, current_user_name, ko_token, org_content, task_id, book_title, failure_personnel, success_personnel, completed_personnel
    # 登录打卡
    if len(sys.argv) > 1:
        isLogin = sys.argv[1]
    else:
        isLogin = 0
    # headers = { 'Accept': 'application/json, text/plain, /', 
    #            'Authorization': 'Bearer 154d2883-70e8-4a46-b906-b4bf99b26438', 
    #            'Origin': 'https://ims.xinchao.com', 
    #            'Referer': 'https://ims.xinchao.com/', 
    #            'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 16_6 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.6 Mobile/15E148 Safari/604.1',
    #            }

    # response = requests.get('https://imsapi.xinchao.com/hraccount/api/clockIn/getDetail?task_id=9&date=2024-05-22&type=0', headers=headers) 
    # print(response.json())
    userList = getUserList()

    current_user_name = userList[0]["name"]
    ko_token = userList[1]["ko_token"]
    to_mail_user = userList[0]["email"]
    book_name = ''

    # 当次打卡失败的人
    failure_personnel = []
    # 登录失败的人
    login_failure_personnel = []
    # 当次打卡成功的人
    success_personnel = []
    # 已经完成打卡的人
    completed_personnel = []
    
    for user in userList:
        ko_token = user["ko_token"]
        to_mail_user = user["email"]
        current_user_name = user["name"]
        userName = user.get("userName")
        if userName != None:
            password = user["password"]
            # 配置了账号密码的人，登录打卡任务才登录, 否则不打卡
            if isLogin == '1':
                try:
                    if datetime.now().day == 1:
                        # 每个月1号修改一次密码
                        login.changePassword(userName, password)
                    ko_token = login.get_token(userName, password)
                except Exception as e:
                    login_failure_personnel.append(current_user_name)
                    print(f"{current_user_name} 登录失败！！！")
            else:
                continue
            
        today_task_list = get_today_task_list()

        if today_task_list is None:
            continue
        for task in today_task_list:
            if task is None:
                continue
            if task["status"] != 1:
                # 任务未开始
                continue
            task_id = task["task_id"]
            if task_id != None: 
                book_name = task['book_name']
                book_info = get_book_info()
                if book_info is None:
                    continue
                book_title = book_info["task_title"]
                org_content = book_info["task_content"]
                if book_info["task_status"] != 10:
                    completed_personnel.append(current_user_name)
                    print(f"{current_user_name} 今日已打卡\n")
                    continue
                # utc_dt = datetime.utcnow().replace(tzinfo=timezone.utc)
                # bj_dt = utc_dt.astimezone(timezone(timedelta(hours=8)))
                # print(f"当前时间：{bj_dt}")
                # if bj_dt.hour < 8:
                #     send_book_content(title=book_title, content=org_content)
                #     thoughts.write_thoughts(org_content, "一")
                #     thoughts.write_thoughts(org_content, "二")

            text = get_content(org_content)
            submit_read_book(
                task_id=task_id,
                text=text,
                to_mail_user=to_mail_user,
            )
# 登录打卡发送读书内容
    if isLogin == '1' and len(org_content) != 0 and len(book_name) != 0:
        send_book_content(title=f"{book_name} {book_title}", content=org_content)
        thoughts.write_thoughts(f"{book_name} {book_title}\n{org_content}", "一")
        thoughts.write_thoughts(f"{book_name} {book_title}\n{org_content}", "二")
    msg = ''
# 有人打卡成功发送打卡成功提示
    if len(success_personnel) != 0:
        msg = f"打卡成功人员名单：\n" +"，".join(success_personnel) + "\n\n已打卡人员名单：\n"+"，".join(completed_personnel)+"\n\n"
# 全部打卡失败或者有人成功且有人失败的时候发送失败提示
    if (len(failure_personnel) != 0 and len(success_personnel) != 0) or len(failure_personnel) == len(userList):
        msg = msg + f"打卡失败人员名单：\n" + "，".join(failure_personnel)

    if len(login_failure_personnel) != 0:
        msg = msg + f"登录失败人员名单：\n" + "，".join(login_failure_personnel)
    if len(msg) != 0:
        msg = msg + '\n\n很遗憾！目前token无法稳定保活，该打卡项目暂停维护。如需继续使用token打卡的同学自己多注意token是否过期，过期后需手动更新token。'
        tools.send_message(msg=msg)
    
def send_book_content(title, content):
    if len(content) != 0:
        h = etree.HTML(content)
        s = f"{title}\n"
        d = h.xpath("//*")
        for i in d:
            if i.tag == "p" or i.tag == "section":
                s = s + "\n\n"
            if i.tag.startswith('h'):
                s = s + '\n'
            if i.text != None:
                s = s + i.text
        print(f"本期读书内容：\n{s}")
        tools.send_message(subject="本期读书内容：", msg=s)

def get_today_task_list():
    url = "https://imsapi.xinchao.com/hraccount/api/clockIn/getBookList"
    res = sendRequest(mothd='get' ,url=url, data=None)
    return get_request_result(res)


# # 获取读书列表
# def getReadingList():
#   print("获取读书列表:")
#   url = 'https://appdpeopqzr1047.h5.xiaoeknow.com/xe.micro_page.h5_more/1.0.0'
  
#   body = {
#     "buz_data": {
#           "id": "punch_card",
#           "page_num": 1,
#           "page_size": 3,
#           "component_id": 19127468,
#           "alive_type": ""
#       }
#   }
#   res = post(url=url, data=body)
#   component = get_request_result(res)
#   bookList = component['list']
#   return bookList

def get_book_info():
    url = "https://imsapi.xinchao.com/hraccount/api/clockIn/getDetail"
    # 获取当前年月日
    utc_dt = datetime.utcnow().replace(tzinfo=timezone.utc)
    bj_dt = utc_dt.astimezone(timezone(timedelta(hours=8)))
    body = {
        "task_id": task_id,
        "type": 0,
        "date": bj_dt.strftime("%Y-%m-%d")
  }
    res = get(url=url, data=body)
    book = get_request_result(res)
    if book is None:
        return book
    theme_info = book["task"]
    return theme_info


def get_content(content):
    if len(org_content) == 0:
        reading_checklist = [
            '阅读让我获得知识，拓展视野，提升思维，丰富心灵，让我成为更好的自己。',
            '每天抽出时间读书，汲取知识，丰富内心，提升自我，成为更优秀的自己。',
            '每日一书，知识增长，心灵滋养，成长不止。'
    "读书是一种享受，每天阅读，不断丰富自己。",
    "阅读是一种力量，每天坚持，必将收获成长。",
    "读书可以开拓视野，每天阅读，不断拓宽自己的世界。",
    "阅读可以陶冶情操，每天阅读，心灵更加美好。",
    "读书可以提高思维，每天阅读，思维更加敏捷。",
    "阅读可以激发灵感，每天阅读，创造力更加出众。",
    "读书可以增加知识，每天阅读，不断丰富自己的智慧。",
    "阅读可以提高语言能力，每天阅读，语言表达更加流畅。",
    "读书可以提高写作能力，每天阅读，写作水平更上一层楼。",
    "阅读可以提高人际交往能力，每天阅读，人际关系更加和谐。"
]
        return random.choice(reading_checklist)
    
    pattern = re.compile(r"<[^>]+>|", re.S)
    result = pattern.sub("", content)
    content_list = result.split("。")
    text = ""
    while not (20 <= len(text) <= 200):
        text = content_list[random.randint(0, len(content_list) - 1)]
        if "nbsp" in text:
            text = ""
    text += "。"
    print(f"打卡内容：{text}\n")
    return text


def submit_read_book(task_id, text, to_mail_user):
    url = "https://imsapi.xinchao.com/hraccount/api/clockIn/sumbitClockIn"
    # 获取当前年月日
    utc_dt = datetime.utcnow().replace(tzinfo=timezone.utc)
    bj_dt = utc_dt.astimezone(timezone(timedelta(hours=8)))
    body = {
        "task_id": task_id,
        "content": text,
        "date": bj_dt.strftime("%Y-%m-%d")
    }
    
    res = post(url=url, data=body)
    r = get_request_result(res)
    if r is None:
        return r
    # send_email_to_user(msg=f"打卡成功！！！<br>本期读书内容：<br>{org_content}", to_user=to_mail_user)
    success_personnel.append(current_user_name)
    print(f"{current_user_name}打卡成功\n")
def getHeader():
    headers = {
            "Authorization": f"Bearer {ko_token}",
            "Content-Type": "application/json",
            "Origin": "https://ims.xinchao.com",
            "Referer": "https://ims.xinchao.com/",
            'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 16_6 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.6 Mobile/15E148 Safari/604.1'
        }
    return headers

def post(url, data):
    try:
        headers = getHeader()
        bodys = json.dumps(data)
        print(f"请求地址：{url}\n")
        print(f"请求参数：{bodys}\n")
        print(f"请求头：{json.dumps(headers)}\n")
        data = bodys.encode()
        res = requests.post(url=url, headers=headers, data=data)
        return res
    except Exception as e:
        failure_personnel.append(current_user_name)
        print(f"{current_user_name} 打卡失败！！！")
def get(url, data):
    try:
        headers = getHeader()
        print(f"请求地址：{url}\n")
        print(f"请求参数：{data}\n")            
        print(f"请求头：{json.dumps(headers)}\n")
        res = requests.get(url=url, headers=headers, params=data)
        return res
    except Exception as e:
        failure_personnel.append(current_user_name)
        print(f"{current_user_name} 打卡失败！！！")

def sendRequest(mothd,url, data):
    if mothd == 'get':
        return get(url=url, data=data)
    else:
        return post(url=url, data=data)

def get_request_result(res):
    try:
        res = json.loads(res.text)
        code = res["code"]
        msg = res["msg"]
        data = res["data"]
        print(f"请求结果：{res}\n")
        if code != '000':
            failure_personnel.append(current_user_name)
            return None
        else:
            return data
    except Exception as e:
        failure_personnel.append(current_user_name)
        print(f"{current_user_name} 打卡失败！！！")

def getUserList()-> list:
   # 读打开文件
    # with open("user_data.json", encoding="utf-8") as a:
    #      # 读取文件
    #     result = json.load(a)
    #     return result
    url = 'https://api.github.com/repos/Tang910103/read-book-task/contents/user_data.json'
    headers = {
        'Authorization': f'Bearer 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',
        'Accept': 'application/vnd.github.v3.raw',
    }
    
    response = requests.get(url, headers=headers)
    
    if response.status_code == 200:
        return json.loads(response.text)
    else:
        print(f'获取用户列表失败: {response.text}')
        raise Exception(f'获取用户列表失败: {response.text}')


if __name__ == "__main__":

    main()
    # schedule.every().day.at("08:00").do(main) # 每天8:00执行一次
    # schedule.every(1).minutes.do(main)
    # 每5分钟执行一次main，中午12点后停止
    # schedule.every(5).minutes.do(main)
    # # schedule.every(6).hours.at(":30").do(main)
    # utc_dt = datetime.utcnow().replace(tzinfo=timezone.utc)
    # bj_dt = utc_dt.astimezone(timezone(timedelta(hours=8)))
    # print(f"当前时间：{bj_dt}")
    # while True:
    #     utc_dt = datetime.utcnow().replace(tzinfo=timezone.utc)
    #     bj_dt = utc_dt.astimezone(timezone(timedelta(hours=8)))
    #     print(f"当前时间：{bj_dt}")
    #     schedule.run_pending()
    #     time.sleep(1)
