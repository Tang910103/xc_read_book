import requests
import json

def send_message(msg, mentioned_list = ["@all"], subject="读书打卡提醒"):
    sed_url = "https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=907ce6be-f7fe-4e28-a661-88073ac9a80e"
    headers={"Content-type": "application/json"}
    data={
    "msgtype": "text",
    "text": {
        "content": f"{subject}\n\n{msg}",
        "mentioned_list":mentioned_list,
        }
    }
    res = requests.post(url=sed_url, headers=headers, data=json.dumps(data))
    print(res.json())
