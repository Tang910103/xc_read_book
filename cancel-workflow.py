import requests
import json
import sys

# 这些变量通常是由GitHub Actions环境提供的，您需要根据实际情况设置它们
# REPO = sys.argv[0]
# WORKFLOW = sys.argv[1]
RUN_ID = sys.argv[1]
# GITHUB_TOKEN = sys.argv[3]
REPO = 'Tang910103/read-book-task'
WORKFLOW = 'read book task'
# RUN_ID = 9262825374
GITHUB_TOKEN = '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'
# 构建API请求的headers
headers = {
    "Accept": "application/vnd.github.v3+json",
    "Authorization": f"token {GITHUB_TOKEN}"
}

# 获取所有正在运行的工作流实例（除去当前运行）
response = requests.get(
    f"https://api.github.com/repos/{REPO}/actions/runs?per_page=100&workflow={WORKFLOW}&branch=main&status=in_progress",
    headers=headers
)

# 检查请求是否成功
if response.status_code == 200:
    data = response.json()
    runs = [run['id'] for run in data['workflow_runs']]
    print(f"执行中的工作流 {runs}")
    # 取消之前的工作流运行（如果需要）
    for run_id in runs:
        if f"{run_id}" == f"{RUN_ID}":
            continue
        cancel_url = f"https://api.github.com/repos/{REPO}/actions/runs/{run_id}/cancel"
        cancel_response = requests.post(cancel_url, headers=headers)
        # 可选：打印出取消操作的结果
        print(f"Attempted to cancel run {run_id}. Status code: {cancel_response.status_code}")
else:
    print(f"Failed to fetch runs. Status code: {response.status_code}")
    print(response.text)