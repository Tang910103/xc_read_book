import json
import requests
import uuid
import encrypt


def get_api_prefix():
    return f"https://ser-cloud.xinchao.com/portal"
def get_host():
    return get_api_prefix().split('/')[2]

def get_rsa_public_key(client_id):
    
    # 获取API前缀
    prefix = get_api_prefix()
    # 构造URL
    url = f"{prefix}/md/encrypt/getRSAPublicKey?clientId={client_id}"
    # 发起GET请求
    response = requests.get(url, headers={'Content-Type': 'application/json','Host': get_host()})
    print(f"url: {url} \nresponse: {response.text}\n")
    response = response.json()
    if response['code'] != '000':
        raise ValueError('getRSAPublicKey：',response)
    # 返回响应对象，你可以根据需要处理这个响应，比如返回响应内容response.text或响应状态码response.status_code
    return response['data']

def get_aes_key(params={}):
    prefix = get_api_prefix() 
    url = f"{prefix}/md/encrypt/getAESKey"
    # 设置请求的头部信息
    headers = {
    "content-type": "application/json",
    'Host': get_host(),
    }
    # 发送POST请求
    response = requests.post(url, json=params, headers=headers)
    res = response.json()
    print(f"url: {url} \nresponse: {res}\n")
    if res['code'] != '000':
        raise ValueError('getRSAPublicKey：',res)
    return res['data']  

def changePassword(userName: str, password: str):
    url = f'{get_api_prefix()}/md/user/changePassword'
    res = requests.post(
        url,
        json= {
            "username":userName,
            "oldPwd": password,
            "newPwd": password,
            "confirmNewPwd": password
            },
        headers= {
            "content-type": "application/json;charset=UTF-8",
            'Host': get_host(),
            },
    ).json()
    print(f"url: {url} \nresponse: {res}\n")
    if res['code'] != '000':
        raise ValueError('getRSAPublicKey：',res)

def get_token(userName: str, password: str):
    
    client_id = uuid.uuid4()
    print(f"client_id: {client_id}")
    # 根据 uuid 从服务端获取 pubkey
    pub = get_rsa_public_key(client_id)
    pub = '\n'.join([
        '-----BEGIN PUBLIC KEY-----',
        pub,
        '-----END PUBLIC KEY-----'
        ])
    
    currentKey = encrypt.get_rsa_keys()
    privateKey  = (currentKey[0])
    publicKey: str = (currentKey[1]).rstrip()
    
    # 用服务端的 public key 加密前端生成的 rsa 公钥
    c = encrypt.encrypt_long(publicKey, pub)
    pr = { 'clientId': f'{client_id}', 'data': encrypt.hex2b64(encrypt.bytes_to_hex(c)) }
    aes_key = get_aes_key(params=pr)
    aesKey = encrypt.rsa_decrypt(aes_key, privateKey)

    data = {
        "username": userName,
        "password": password
    }

    # 将字典转换为JSON字符串，并确保它与plaintext相同
    plaintext = json.dumps(data,).replace(' ', '')
    encryptedParams = encrypt.aes_encrypt(plaintext, aesKey)
    url = f'{get_api_prefix()}/pl/pack/login/usernameLoginWithEncrypt'
    res = requests.post(
        url,
        json={
            'encryptInfo': {
            'encryptInfo': encryptedParams,
            'clientId': f"{client_id}",
            },
        },
        headers= {
            "content-type": "application/json;charset=UTF-8",
            'Host': get_host(),
            },
    ).json()
    print(f"url: {url} \nresponse: {res}\n")
    if res['code'] != '000':
        raise ValueError('getRSAPublicKey：',res)
    return res['data']['accessToken'] 