import json
from openai import OpenAI
from datetime import datetime, timezone, timedelta
import tools
import os


client = OpenAI(
    api_key="sk-QidAa9Hr4UVZ6rZAt255i65lEQbNobsiSbFYCrXhXxPwEvB8",
    base_url="https://api.chatanywhere.com.cn"
                )

def generate_thoughts(prompt, index):
    
    try:
            # Set the parameters for the GPT-3 model
        model_engine = "gpt-3.5-turbo"
        max_tokens = 2048
        temperature = 0.5
        top_p = 1
        messages=[
            {"role": "system", "content": prompt},
            {"role": "system", "content": "以公司员工的身份，阅读上面文章，写篇200字左右家书，家书内容需要体现阅读文章的收获，家书不需要署名。"},
            ]
        
        # Generate the first letter
        letter_1 = client.chat.completions.create(
                    model=model_engine,
                    messages=messages,
                    temperature=temperature,
                    max_tokens=max_tokens,
                    top_p=top_p,
                )
    
        comment_messages=[
            {"role": "system", "content": "点评格式：点评x：(换行)感谢你的精彩分享！(此处写点评内容)"},
            {"role": "system", "content": "以同事的身份，用前面给出的点评格式，写四条家书点评，每条30字左右，家书如下:\n" + letter_1.choices[0].message.content},
            ]
        comment_1 = client.chat.completions.create(
                    model=model_engine,
                    messages=comment_messages,
                    temperature=temperature,
                    max_tokens=max_tokens,
                    top_p=top_p,
                )


        # Construct the letters
        utc_dt = datetime.utcnow().replace(tzinfo=timezone.utc)
        bj_dt = utc_dt.astimezone(timezone(timedelta(hours=8)))
        date = bj_dt.strftime("%Y年%m月%d日")
        letter_1_text = f"家书{index}：\n{date}家书：各位家人们，早上好! \n{letter_1.choices[0].message.content}\n\n{comment_1.choices[0].message.content}"
        return letter_1_text
    except Exception as e:
        print(f"写家书失败：{e}")


def write_thoughts(result, index):
    letter_1 = generate_thoughts(result, index)

    print(f"{letter_1}\n")
    if letter_1 != None and len(letter_1) != 0:
        tools.send_message(subject='本期读书感悟', msg=letter_1)
