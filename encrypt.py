
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa as rsae
from cryptography.hazmat.backends import default_backend
import base64
import rsa.pem
import rsa
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad

# 生成本地的 RSA 密钥对
def get_rsa_keys():
    # 生成RSA密钥对
    private_key = rsae.generate_private_key(
        public_exponent=65537,
        key_size=2048,
        backend=default_backend()
    )
    # 导出私钥（PKCS#8格式）
    private_key_pem = private_key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption()
    )
    # 将 PEM 格式转换为字符串
    pem_str = private_key_pem.decode('utf-8')
    
    # 导出公钥（SPKI格式）
    public_key = private_key.public_key()
    public_key_pem = public_key.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
    )

    # (public_key,private_key) = rsa.newkeys(2048)
    # public_key_pem = public_key.save_pkcs1()
    # private_key_pem = private_key.save_pkcs1()
    # 去掉-----BEGIN PUBLIC KEY-----和-----END PUBLIC KEY-----
    privateKey = private_key_pem.decode('utf-8')
    # privateKey = privateKey.replace("-----BEGIN PRIVATE KEY-----", "")
    # privateKey = privateKey.replace("-----END PRIVATE KEY-----", "")
    # privateKey = privateKey.replace("\n", "")

    publicKey = public_key_pem.decode('utf-8')
    publicKey = publicKey.replace("-----BEGIN PUBLIC KEY-----", "")
    publicKey = publicKey.replace("-----END PUBLIC KEY-----", "")
    publicKey = publicKey.replace("\n", "")
    print(pem_str)
    print()
    # 返回私钥和公钥
    return [privateKey, publicKey]

def hex2b64(hex_str):
    # 使用Python的内置base64库来转换十六进制字符串到Base64
    # 首先将十六进制字符串转换为字节
    binary_data = bytes.fromhex(hex_str)
    # 然后将字节编码为Base64字符串
    b64_data = base64.b64encode(binary_data).decode('utf-8')
    return b64_data

def hex_to_bytes(hex_str):
    bytes_list = [int(hex_str[i:i+2], 16) for i in range(0, len(hex_str), 2)]
    return bytes_list

def bytes_to_hex(bytes_list):
    hex_list = ['{:02x}'.format(byte) for byte in bytes_list]
    return ''.join(hex_list)

b64map = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
b64pad = "="
def hex2b64(h):
    ret = ""
    
    for i in range(0, len(h), 3):
        # 处理每3个字符的块
        if i + 3 <= len(h):
            c = int(h[i:i+3], 16)
            ret += b64map[c >> 6] + b64map[c & 0x3F]
        else:
            # 处理剩余的字符
            remaining = len(h) - i
            if remaining == 1:
                c = int(h[i:], 16)
                ret += b64map[c << 2]
            elif remaining == 2:
                c = int(h[i:i+2], 16)
                ret += b64map[c >> 2] + b64map[(c & 0x03) << 4]
    
    # 添加填充字符直到字符串长度为4的倍数
    while len(ret) % 4:
        ret += b64pad
    
    return ret

def encrypt_long(plaintext, pub: str)-> bytes:
    pub_key=rsa.PublicKey.load_pkcs1_openssl_pem(pub.encode('utf-8'))
    # 获取最大长度
    max_length = rsa.common.byte_size(pub_key.n) - 11
    # 如果明文长度大于最大长度，需要分割明文
    if len(plaintext) > max_length:
        chunks: list[str] = [plaintext[i:i + max_length] for i in range(0, len(plaintext), max_length)]
        encrypted_chunks: list[bytes] = []
        for chunk in chunks:
            # 对每个块进行填充
            padded_chunk = chunk.encode('utf-8')
            # 加密每个块
            encrypted_chunk = rsa.encrypt(message=padded_chunk, pub_key=pub_key)
            encrypted_chunks.append(encrypted_chunk)
        # 将所有加密块拼接起来
        return b''.join(encrypted_chunks)
    else:
        # 如果明文长度小于等于最大长度，直接加密
        encrypted_chunk = rsa.encrypt(message=plaintext.encode('utf-8'), pub_key=pub_key)
        return encrypted_chunk
# 定义Base64字符集和填充字符
b64map = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
b64pad = "="
BI_RM = "0123456789abcdefghijklmnopqrstuvwxyz"


def int2char(n):
    if 0 <= n < len(BI_RM):
        return BI_RM[n]
    else:
        raise IndexError("Integer out of range for BI_RM")

def b64tohex(s):
    ret = ''
    k = 0  # b64状态，0-3
    for char in s:
        if char == b64pad:
            break
        v = b64map.find(char)
        if v < 0:
            continue
        if k == 0:
            ret += int2char(v >> 2)
            slop = v & 0x03
            k = 1
        elif k == 1:
            ret += int2char((slop << 2) | (v >> 4))
            slop = v & 0x0F
            k = 2
        elif k == 2:
            ret += int2char(slop)
            ret += int2char(v >> 2)
            slop = v & 0x03
            k = 3
        else:
            ret += int2char((slop << 2) | (v >> 4))
            ret += int2char(v & 0xf)
            k = 0
    
    # 处理剩余的数据
    if k == 1:
        ret += int2char(slop << 2)

    return ret

def rsa_decrypt(biz_content, private_key):
    _pri = rsa.PrivateKey._load_pkcs1_pem(private_key)
    biz_content = base64.b64decode(biz_content.encode('utf-8'))
    return rsa.decrypt(biz_content, _pri).decode('utf-8')

def get_key(key):
    return (key).encode('utf-8')

def aes_encrypt(text, key):
    key = get_key(key)
    cipher = AES.new(key, AES.MODE_ECB)
    padded_text = pad((text).encode('utf-8'), AES.block_size)
    encrypted_text = cipher.encrypt(padded_text)
    return base64.b64encode(encrypted_text).decode('utf-8')

def aes_decrypt(encrypted_text, key):
    key = get_key(key)
    cipher = AES.new(key, AES.MODE_ECB)
    encrypted_bytes = base64.b64decode(encrypted_text)
    decrypted_bytes = cipher.decrypt(encrypted_bytes)
    return unpad(decrypted_bytes, AES.block_size).decode('utf-8')